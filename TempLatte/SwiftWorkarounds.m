//
//  SwiftWorkarounds.m
//  TempLatte
//
//  Created by Sven Ewers on 10/10/14.
//  Copyright (c) 2014 Sven Ewers. All rights reserved.
//

#import "SwiftWorkarounds.h"

@implementation SwiftWorkarounds
- (NSRegularExpressionOptions)combine:(NSRegularExpressionOptions)op1 with:(NSRegularExpressionOptions)op2{
    return op1 | op2;
}
@end
