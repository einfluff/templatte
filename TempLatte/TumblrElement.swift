//
//  TumblrElement.swift
//  TempLatte
//
//  Created by Sven Ewers on 10/10/14.
//  Copyright (c) 2014 Sven Ewers. All rights reserved.
//

import Foundation
class TumblrElement {
    //list of all possible child-elements an element can have
    //#TODO: Remove this code. It's slow and it WILL break!
    let possibleChildBlockTypes = ["Title", "Caption", "Source", "Lines", "Label"]
    
    //the actual element
    var type :String = ""
    var content :String = "" //Will contain ChildElements
    
    //One elements child-elements
    var childElements: [TumblrElement] = []
    
    //Is this element to be considered upon rendering the posts block
    var isPostType = false
    
    init(newType :String, newContent: String){
        
    }
}