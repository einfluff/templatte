//
//  MainWindowController.swift
//  TempLatte
//
//  Created by Sven Ewers on 10/10/14.
//  Copyright (c) 2014 Sven Ewers. All rights reserved.
//

import Foundation
import Cocoa



class MainWindowController : NSObject, NSWindowDelegate {
    @IBOutlet weak var window: NSWindow!
    @IBOutlet var editor: NSTextView!

    //The actuall templating magic
    var tumplate: TumblrTemplate = TumblrTemplate()
    
    @IBAction func parseAndProcess(AnyObject){
        tumplate = TumblrTemplate(content: editor.string!)
        tumplate.parse()
    }
}