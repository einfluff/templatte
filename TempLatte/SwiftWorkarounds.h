//
//  SwiftWorkarounds.h
//  TempLatte
//
//  Created by Sven Ewers on 10/10/14.
//  Copyright (c) 2014 Sven Ewers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SwiftWorkarounds : NSObject
- (NSRegularExpressionOptions)combine:(NSRegularExpressionOptions)op1 with:(NSRegularExpressionOptions)op2;
@end
