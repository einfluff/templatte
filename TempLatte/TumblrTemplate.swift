//
//  Template.swift
//  TempLatte
//
//  Created by Sven Ewers on 10/10/14.
//  Copyright (c) 2014 Sven Ewers. All rights reserved.
//

import Foundation

class TumblrTemplate{
    //The actual template
    var source:String = ""
    
    //The actual template - splitted up into body and header
    //This needs to happen because some {Block:Type} get used in multiple places (Posts and in the header itself)
    var sourceHead :String = ""
    var sourceBody :String = ""
    
    //Highest level of possible body block types 
    //(counting Posts and it's childs as one level, because convenience
    let bodyBlockTypes = ["Description", "Posts", "Text", "Photo", "Panorama",
        "Photoset", "Quote", "Link", "Chat", "Video", "Audio", "PreviousPage", "NextPage"]
    
    //On runtime storage
    var elements: [TumblrElement] = []
    
    //Parsing basics (sort out the template code)
    func parse() -> Bool{
        println("Parsing")
        sourceHead = getTag(source, tag: "head")
        sourceBody = getTag(source, tag: "body")
        
        parseBodyBlocks()
        return true
    }
    
    func parseBodyBlocks() {
        for bType in bodyBlockTypes {
            //Escape the enclosing block
            //#TODO: Remove function, build preset string, save processing
            let blockOpen = "{block:" + bType + "}"
            let blockClose = "{/block:" + bType + "}"
            
            var workaround: SwiftWorkarounds = SwiftWorkarounds()
            let regexoption = workaround.combine(NSRegularExpressionOptions.CaseInsensitive, with: NSRegularExpressionOptions.DotMatchesLineSeparators)
            
            let escapedOpen = NSRegularExpression.escapedPatternForString(blockOpen)
            let escapedClose = NSRegularExpression.escapedPatternForString(blockClose)
            
            let regexp = NSRegularExpression(pattern:".*"+escapedOpen+"(.*)"+escapedClose+".*", options:regexoption, error: nil)
            let result = regexp?.stringByReplacingMatchesInString(sourceBody, options: nil, range: NSRange(location:0, length:countElements(sourceBody)), withTemplate: "$1")
            println("\n Type " + bType + " got result: \n " +  result!)
                    }
    }
    
    //init
    init(content:String){
        source = content
    }
    
    //really stupid init workaround
    init(){
    }
    
    //Helper functions. #TODO: Rewrite more cleanly
    private func getTag(data:String, tag:String) -> String {
        let regexp = NSRegularExpression(pattern: ".*<" + tag + ">(.+)</" + tag + ">.*", options: NSRegularExpressionOptions.DotMatchesLineSeparators, error: nil)
        let result = regexp?.stringByReplacingMatchesInString(data, options: nil, range: NSRange(location:0, length:countElements(data)), withTemplate: "$1")
        return result!
    }
}